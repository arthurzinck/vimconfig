set number
let mapleader = ","

packloadall
set incsearch
set termguicolors
colorscheme slate
nnoremap <C-J> <C-W><C-J>    
nnoremap <C-K> <C-W><C-K>    
nnoremap <C-L> <C-W><C-L>    
nnoremap <C-H> <C-W><C-H> 


call plug#begin()
Plug 'hashivim/vim-terraform'
Plug 'SirVer/ultisnips'
Plug 'honza/vim-snippets'
Plug 'andrewstuart/vim-kubernetes'
Plug 'itchyny/lightline.vim'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-eunuch' 
Plug 'sheerun/vim-polyglot'
Plug 'editorconfig/editorconfig-vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'hashivim/vim-terraform'
Plug 'vim-syntastic/syntastic'
Plug 'APZelos/blamer.nvim'
Plug 'juliosueiras/vim-terraform-completion'
Plug 'google/vim-jsonnet'
"Plug 'airblade/vim-gitgutter.git'
call plug#end()

"set tabstop=2 shiftwidth=2 expandtab
"autocmd Filetype py setlocal tabstop=4

"set mouse=a
let g:terraform_align=1
let g:terraform_fmt_on_save=1
let g:terraform_fold_sections=0
" BlamR
let g:blamer_enabled = 1
let g:blamer_template = '<author-time> <author>: <summary>'
